/*
 
 The visual part of my code has been built using three different codes.
 
 The base of my app is through the MaxiGranular program that was given to us as a reference. I have used the example and modified it so that it could adapt in a better way to my desired output. I have kept the visual that was used as it seemed like a good addition to my desired output.
 
 The background of my final output is built upon an example mentioned in the book of shaders.Originally my plan was to make a generative noise field (something like this one: https://www.youtube.com/watch?v=BjoM9oKOAKY but I was unable to get all the code written in c++. I tried building a particle system from scratch and building the field in openframeworks but I was successful. Ultimately, I was a little hesitant with shaders but managed to pull through and get the example working.
 
 The final aspect of my visual output is the small cloud like formation of the particles. The base code for this has been picked up from the book -
     Link: https://www.packtpub.com/product/mastering-openframeworks-creative-coding-demystified/9781849518048
     Name: Mastering openFrameworks: Creative Coding Demystified
     Publication Date: September 2013
     ISBN: 9781849518048
     Publisher: Packt
     Example Number - 06-Sound/06-DancingCloud
 
 The openframeworks example uses the inbuilt sound function to extract the fft and on the basis of that display the output. In order for me to get it to work, I had to understand the code first and then link it with the maximillian library.
 
 
 The audio part of my code has been built using the maximillian library and the code I wrote for the 4 small coursewrok assignments. I have used the code from both the FM_Synth and Subtractive Synth code files.
 */

#include "ofApp.h"
#include "time.h"

const int n = 100;
const int N = 256;        //Number of bands in spectrum

//Offsets for Perlin noise calculation for points
float pos_x[n], pos_y[n];
ofPoint p[n];            //Cloud's points positions
float spectrum[ N ];    //Smoothed spectrum values

//--------------------------------------------------------------
void ofApp::setup(){
    
    ofBackground(0);
	ofDisableArbTex();
	ofEnableNormalizedTexCoords();
//    ellipseMeshBackground.setup();
	surfaceShader.load("simpleTexturedMesh");
    
    //using camera to make a mesh
	myCamera.setPosition(vec3(0.0f, 8.0f, 40.0f));
	myCamera.setTarget(vec3(0.0f, 0.0f, 0.0f));
	myCamera.setFov(32.0f);
	myCamera.setNearClip(0.005f);
	myCamera.setFarClip(1000.0f);
	myCamera.setAutoDistance(false);
    
    //sample from
    //Music by <a href="/users/aldermansweden-21004879/?tab=audio&amp;utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=audio&amp;utm_content=3938">aldermansweden</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=music&amp;utm_content=3938">Pixabay</a>
    //https://pixabay.com/music/
    samp.load(ofToDataPath("/Users/aananditagupta/Desktop/AV/Programs/CW2/E_ofx_dancing_cloud/bin/data/2.wav"));
    
    //samples from freesound.org
    samp2.load(ofToDataPath("/Users/aananditagupta/Desktop/AV/Programs/CW2/Maximilian/synth.wav"));
//    samp3.load(ofToDataPath("/Users/aananditagupta/Desktop/AV/Programs/CW2/Maximilian/wingtai_kick.wav"));
    
    int sampleRate = 44100; /* Sampling Rate */
    int bufferSize = 512; /* Buffer Size. you have to fill this buffer with sound using the for loop in the audioOut method */

    samples.push_back(new maxiTimePitchStretch<grainPlayerWin, maxiSample>(&samp));
//    stretches.push_back(new maxiTimePitchStretch<grainPlayerWin, maxiSample>(&samp2));
    
    speed = 1;
    grainLength = 0.5;
    current = 0;

    featureextractor.setup(1024, 512, 256);
    octaveanalyser.setup(sampleRate, 1024, 10);

    ofxMaxiSettings::setup(sampleRate, 2, bufferSize);

    // of Sound setup
    ofSoundStreamSettings settings;
    auto devices = soundStream.getMatchingDevices("default");

    if (!devices.empty()) {
        settings.setInDevice(devices[0]);
    }

    settings.setInListener(this);
    settings.setOutListener(this);
    settings.sampleRate = sampleRate;
    settings.numOutputChannels = 2;
    settings.numInputChannels = 0;
    settings.bufferSize = bufferSize;
    soundStream.setup(settings);
    
    //setting up the compressor
    compressor.setAttack(100);
    compressor.setRelease(3000);
    compressor.setThreshold(0.50);
    compressor.setRatio(2);
    
    //this is to ensure there is short attack and a longer release and sustain
    //so that the sound can keep playing till the next attack hits
    ADSR.setAttack(100);
    ADSR.setDecay(1);
    ADSR.setSustain(1000);
    ADSR.setRelease(10000);
    
    for (int i=0; i<N; i++) {
        spectrum[i] = 0.0f;
    }

    //Initialize points offsets by random numbers
    for ( int j=0; j<n; j++ ) {
        pos_x[j] = ofRandom( 0, 100 );
        pos_y[j] = ofRandom( 0, 100 );
    }
}

//--------------------------------------------------------------
void ofApp::update(){
    
    //Computing dt as a time between the last
    //and the current calling of update()
    float time = ofGetElapsedTimef();
    float dt = time - time0;
    dt = ofClamp( dt, 0.0, 0.1 );
    time0 = time; //Store the current time

    //Update Rad and Vel using the MaxiFFT Octave Analyser function
    Rad = ofMap( octaveanalyser.averages[ bandRad ], 1, 3, 200, 800, true );
    Vel = ofMap( octaveanalyser.averages[ bandVel ], 0, 0.1, 0.05, 0.5 );
    
    
    //Updatde particles positions
    for (int j=0; j<n; j++) {
        pos_x[j] += Vel * dt;    //move offset
        pos_y[j] += Vel * dt;    //move offset
        //Calculate Perlin's noise in [-1, 1] and
        //multiply on Rad
        p[j].x = ofSignedNoise( pos_x[j] ) * Rad ;
        p[j].y = ofSignedNoise( pos_y[j] ) * Rad;
    }
    
    //changing the value of the hit every time so that we can get a slightly altered beat every time
    for(int i = 0; i < 16 ; i++)
    {
        hit[i] = ofNoise(0.06, i * 0.05, time) * 5;
    }
    
    //setting the box width and height for the noise field at the back
    box.setWidth(ofGetWidth());
    box.setHeight(ofGetHeight());
}
//--------------------------------------------------------------
void ofApp::draw(){
    
    //this is the integration between the shader and openframeworks.
	ofEnableDepthTest();
	myCamera.begin();
    
	surfaceShader.begin();
    
    //we are passing three important variables to the shader
    //1. screen resolution
    //2. mouse position
    //3. the elapsed time
    surfaceShader.setUniform2f("u_resolution", ofGetWidth(), ofGetHeight());
    surfaceShader.setUniform2f("u_mouse", ofGetMouseX(), ofGetMouseY());
    surfaceShader.setUniform1f("u_time", ofGetElapsedTimef());
    
    //the output of the shader gets drawn on the box
    box.draw();

	surfaceShader.end();
    
    myCamera.end();
    
    //drawing the cloud like particle system
    //Move center of coordinate system to the screen center
    ofPushMatrix();
    ofTranslate( ofGetWidth() / 2, ofGetHeight() / 2 );
//    ofDrawCircle(0, 0, 100);
    
    //Draw points
    ofSetColor(ofRandom(0,20)); //ofRandom(255)/bandRad, ofRandom(255)/bandVel, ofRandom(255)/bandRad );
    ofFill();
    for (int i=0; i<n; i++) {
        ofCircle( p[i], 5 );
    }

    //Draw lines between near points
    float dist = 20;    //Threshold parameter of distance
    for (int j=0; j<n; j++) {
        for (int k=j+1; k<n; k++) {
            if ( ofDist( p[j].x, p[j].y, p[k].x, p[k].y )
                < dist ) {
                    ofLine( p[j], p[k] );
            }
        }
    }

    //Restore coordinate system
    ofPopMatrix();
    
    ofPushMatrix();
    ofTranslate( ofGetWidth() / 2, ofGetHeight() / 2 );
    ofNoFill();

    for (int i = 0; i < octaveanalyser.nAverages; ++i) {
        ofSetColor(20 + ((int)(ofGetFrameNum()) % 255),
            10 + ((int)(ofGetFrameNum() * 1.4) % 255),
            ofGetFrameNum() % 255,
                   octaveanalyser.averages[i] / 20.0 * 255.0);
        glPushMatrix();
        glTranslatef(ofGetWidth() / 2, ofGetHeight() / 2, 0);
        glRotatef(0.01 * ofGetFrameNum() * speed * i/2, 0.01 * ofGetFrameNum() * speed * i/2, 0.01 * ofGetFrameNum() * speed * i/2, 0);
        ofDrawSphere(0, 0, i * 5);
        glPopMatrix();
    }
    ofPopMatrix();

}

//--------------------------------------------------------------
void ofApp::audioOut(ofSoundBuffer& output) {
    std::size_t nChannels = output.getNumChannels();
    snaretrigger = hit[playHead%16];//same for the snare
    playHead++;//iterate the playhead
    for (std::size_t i = 0; i < output.getNumFrames(); i++) {
        wave = samples[current]->play(1., speed, grainLength, 4, 0);

        if (featureextractor.process(wave)) {
            octaveanalyser.calculate(featureextractor.magnitudes);
        }
        
        if (snaretrigger==1) {

            samp2.trigger();//likewise for the snare
//            samp3.trigger();
        }
        
        //and this is where we build the synth
        
        ADSRout=ADSR.adsr(1.0,ADSR.trigger);
        
        
        //I have altered the code to adapt to build a monophonic subtractive synth using additive synthesis
        //lores filter and an increase in both the frequency and the amplitude of the sound.
        LFO1out=LFO1.sinebuf(0.6);//this lfo is a sinewave at 0.6 hz
        
        VCO1out=VCO1.pulse(550,0.2);//here's VCO1. it's a pulse wave at 550 hz, with a pulse width of 0.2
        VCO2out=VCO2.pulse(110+LFO1out,0.2);//here's VCO2. it's a pulse wave at 110hz with LFO modulation on the frequency, and width of 0.2
        
        LF01Output = myLF01.sinewave(5) * 3;
        LF02Output = myLF02.sinewave(0.5) * 2000;
        
        oscOutput = myOsc.sawn(myPortamento.lopass(ADSRout, 0.001));
        
        VCFout=VCF.lores((VCO1out+VCO2out)*0.5, ADSRout*10000, 10);//now we stick the VCO's into the VCF, using the ADSR as the filter cutoff
        double finalSound=(VCFout * ADSRout * oscOutput) * 0.8;//finally we add the ADSR as an amplitude modulator
        
        double sampleOut = compressor.compress(samp2.playOnce());
        //play result
        mymix.stereo(wave, outputs, 0.5);
        output[i * nChannels] = output[0] + sampleOut + finalSound; //compressor.compress(samp.play()) + samp2.playOnce(); /* You may end up with lots of outputs. add them here */
        output[i * nChannels + 1] = outputs[1] + ( mySine.sinewave(440) * myOtherSine.sinewave(20) * myOtherOtherSine.triangle(30) ) * 0.2 + sampleOut;
    }
}

//--------------------------------------------------------------
void ofApp::audioIn(ofSoundBuffer& input) {
    std::size_t nChannels = input.getNumChannels();
    for (std::size_t i = 0; i < input.getNumFrames(); ++i) {
        //handle input here
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
